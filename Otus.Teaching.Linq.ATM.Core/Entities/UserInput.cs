﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
   public class UserInput
   {
      public string Login { get; }
      public string Password { get;}
      public decimal CashAmount { get; }
      public UserInput()
      {
         Login = GetUserInput("login", "star");
         Password = GetUserInput("password", "444");
         CashAmount = ConverrtInputToDecimal(GetUserInput("cash amount", "100550m"), 100550m);
      }
      private string GetUserInput(string propertyName, string defaultValue)
      {
         Console.WriteLine($"Enter {propertyName} (or skip to use default value - {defaultValue}) :");
         string userInput = Console.ReadLine();
         return string.IsNullOrEmpty(userInput) ? defaultValue : userInput;
      }
      private decimal ConverrtInputToDecimal(string input, decimal defaultValue)
         => Decimal.TryParse(input, out decimal converted) ? converted : defaultValue;
   }
}
