﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
   internal static class ATMExtension
   {
      internal static string ConvertToString(this User user)
         => user == null? string.Empty : $"User Id: {user.Id}, " +
                                         $"name: {user.FirstName} {user.MiddleName} {user.SurName}, " +
                                         $"phone: {user.Phone}, " +
                                         $"passport: {user.PassportSeriesAndNumber}, " +
                                         $"registered: {user.RegistrationDate}";

      internal static string ConvertToString(this Account account)
         => $"Account: {account.Id}, opened: {account.OpeningDate}, money left: {account.CashAll}";

      internal static string ConvertToString(this IEnumerable<Account> items)
      {
         var accounts = items.Select(s => s.ConvertToString());
         return accounts.Any() ? accounts.Aggregate((a1, a2) => a1 + "\n" + a2) : string.Empty;
      }

      internal static string ConvertToString(this IEnumerable<OperationsHistory> items)
      {
         var operations = items.Select(s => s.ConvertToString());
         return operations.Any() ? operations.Aggregate((a1, a2) => a1 + "\n" + a2) : string.Empty;
      }

      internal static string ConvertToString(this OperationsHistory hisory)
         => hisory == null ?
                      " - no operation history found" :
                      $" - accountId: {hisory.AccountId}, operationId: {hisory.Id}, date: {hisory.OperationDate}, type: {hisory.OperationType}, sum {hisory.CashSum}";

      internal static string ConvertToString(this Dictionary<Account, List<OperationsHistory>> dictionary)
      {
         StringBuilder builder = new StringBuilder();
         foreach (var entry in dictionary)
         {
            builder.AppendLine(entry.Key.ConvertToString());
            builder.AppendLine(entry.Value.ConvertToString());
         }
         return builder.ToString();
      }

      internal static string ConvertToString(this KeyValuePair<User, OperationsHistory> item)
         => $"UserId: {item.Key.Id}{item.Value.ConvertToString()}";

      internal static string ConvertToString(this IEnumerable<KeyValuePair<User, OperationsHistory>> items)
      {
         var userOperations = items.Select(s => s.ConvertToString());
         return userOperations.Any() ? userOperations.Aggregate((a1, a2) => a1 + "\n" + a2) : string.Empty;
      }

      internal static string ConvertToString(this KeyValuePair<User, decimal> item)
         => $"{item.Key.ConvertToString()} (total accounts sum: {item.Value})";

      internal static string ConvertToString(this IEnumerable<KeyValuePair<User, decimal>> items)
      {
         var userAccounts = items.Select(s => s.ConvertToString());
         return userAccounts.Any() ? userAccounts.Aggregate((a1, a2) => a1 + "\n" + a2) : string.Empty;
      }

      internal static string ConvertToString(this KeyValuePair<User, Account> item)
         => $"{item.Key.ConvertToString()}, accountId: {item.Value.Id}, cash: {item.Value.CashAll})";

      internal static string ConvertToString(this IEnumerable<KeyValuePair<User, Account>> items)
      {
         var userAccounts = items.Select(s => s.ConvertToString());
         return userAccounts.Any() ? userAccounts.Aggregate((a1, a2) => a1 + "\n" + a2) : string.Empty;
      }
   }
}
