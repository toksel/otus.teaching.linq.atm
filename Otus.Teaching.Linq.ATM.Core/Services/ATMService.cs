﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
   public class ATMService
   {
      private readonly ATMManager _manager;

      public ATMService(ATMManager manager)
         => _manager = manager;

      internal User GetUser(string login, string password)
         => _manager.Users.FirstOrDefault(w => w.Login == login && w.Password == password);

      public string GetUserInfo(string login, string password)
         => GetUser(login, password).ConvertToString();

      internal IEnumerable<Account> GetAccounts(User user)
         => user == null ? new List<Account>() : _manager.Accounts.Where(w => w.UserId == user.Id);

      public string GetAccoutsInfo(string login, string password)
         => GetAccounts(GetUser(login, password)).ConvertToString();

      internal IEnumerable<OperationsHistory> GetOperations(Account account)
         => _manager.History.Where(w => w.AccountId == account.Id);

      internal Dictionary<Account, List<OperationsHistory>> GetAccountsWithHistory(User user)
         => GetAccounts(user).GroupJoin(_manager.History,
                                        a => a.Id,
                                        h => h.AccountId,
                                        (a, h) => new { Account = a, History = h })
                             .SelectMany(s => s.History.DefaultIfEmpty(),
                                         (a, s) => new { a.Account, History = s })
                             .GroupBy(g => g.Account)
                             .Select(g => new { Account = g.Key, Hisotry = g.Select( s => s.History).ToList()})
                             .ToDictionary(k => k.Account,
                                           v => v.Hisotry);

      public string GetAccountsWithHistoryInfo(string login, string password)
         => GetAccountsWithHistory(GetUser(login, password)).ConvertToString();

      internal IEnumerable<KeyValuePair<User, OperationsHistory>> GetInputCashOperations()
         => _manager.History.Where(w => w.OperationType == OperationType.InputCash)
                            .Join(_manager.Accounts.Join(_manager.Users,
                                                         a => a.UserId,
                                                         u => u.Id,
                                                         (a, u) => new { AccountId = a.Id, User = u }),
                                  h => h.AccountId,
                                  a => a.AccountId,
                                  (h, a) => new KeyValuePair<User, OperationsHistory>(a.User, h));
      public string GetInputCashOperationsInfo()
         => GetInputCashOperations().ConvertToString();

      internal IEnumerable<KeyValuePair<User, decimal>> GetUsersHavingSummaryMoreCash(decimal sum)
         => _manager.Users.Join(_manager.Accounts,
                                u => u.Id,
                                a => a.UserId,
                                (u, a) => new { User = u, a.CashAll })
                          .GroupBy(gu => gu.User,
                                   ga => ga.CashAll,
                                   (gu, ga) => new { User = gu, CashList = ga.ToList() })
                          .Select(s => new KeyValuePair<User, decimal>(s.User, s.CashList.Sum()))
                          .Where(w => w.Value >= sum);
      public string GetUsersHavingSummaryMoreCashInfo(decimal sum)
         => GetUsersHavingSummaryMoreCash(sum).ConvertToString();

      internal IEnumerable<KeyValuePair<User, Account>> GetUsersHavingAccountWithMoreCash(decimal sum)
         => _manager.Accounts.Where(a => a.CashAll >= sum)
                             .Join(_manager.Users,
                                   a => a.UserId,
                                   u => u.Id,
                                   (a, u) => new KeyValuePair<User, Account>(u, a));

      public string GetUsersHavingAccountWithMoreCashInfo(decimal sum)
         => GetUsersHavingAccountWithMoreCash(sum).ConvertToString();
   }
}
