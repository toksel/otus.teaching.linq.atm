﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            var atmService = new ATMService(atmManager);
            UserInput input = new UserInput();

            // 1. Вывод информации о заданном аккаунте по логину и паролю;
            System.Console.WriteLine($"\n1. Вывод информации о заданном аккаунте по логину ({input.Login}) и паролю ({input.Password});");
            System.Console.WriteLine(atmService.GetUserInfo(login: input.Login, password: input.Password));

            // 2. Вывод данных о всех счетах заданного пользователя;
            System.Console.WriteLine($"\n2. Вывод данных о всех счетах заданного пользователя (логин: {input.Login}, пароль: {input.Password});");
            System.Console.WriteLine(atmService.GetAccoutsInfo(login: input.Login, password: input.Password));

            // 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
            System.Console.WriteLine($"\n3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту (логин: {input.Login}, пароль: {input.Password});");
            System.Console.WriteLine(atmService.GetAccountsWithHistoryInfo(login: input.Login, password: input.Password));

            // 4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
            System.Console.WriteLine($"\n4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;");
            System.Console.WriteLine(atmService.GetInputCashOperationsInfo());

            // 5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
            System.Console.WriteLine($"\n5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой, выбрано {input.CashAmount});");
            // 5.1. У пользователя суммарно на всех аккаунтах сумма больше необходимой;
            System.Console.WriteLine($"\n5.1. У пользователя суммарно на всех аккаунтах сумма больше необходимой ({input.CashAmount});");
            System.Console.WriteLine(atmService.GetUsersHavingSummaryMoreCashInfo(input.CashAmount));
            // 5.2. У пользователя на конкретном аккаунте сумма больше необходимой;
            System.Console.WriteLine($"\n5.2. У пользователя на конкретном аккаунте сумма больше необходимой ({input.CashAmount});");
            System.Console.WriteLine(atmService.GetUsersHavingAccountWithMoreCashInfo(input.CashAmount));

            System.Console.WriteLine("\nЗавершение работы приложения-банкомата...");

            System.Console.ReadKey();
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}